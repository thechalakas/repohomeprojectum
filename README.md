# repohomeprojectum

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to repos related to project UM

**here are the repos**

I have written some code related to project um. I built a content management system that could be used to manage performance of a group of people in a setting such as a hobby club. eventually i plan to make it open source.

Currently there is no code to share but I will do so eventually.

---

1. [https://bitbucket.org/thechalakas]

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 